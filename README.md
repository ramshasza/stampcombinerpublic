# Datenschutzhinweise

## Geltungsbereich

Diese Datenschutzhinweise gelten für die App **Stamp Combiner**, die von **Hass Development** entwickelt wurde.

## Datenerhebung und -verarbeitung

Die App erhebt und verarbeitet keine personenbezogenen Daten. Die vom Nutzer eingegebenen Daten werden lokal auf dem Gerät gespeichert und nicht an Dritte weitergegeben.

## Anforderung zum Löschen von Konten
Nutzer können die App ohne Konto nutzen. Die Kontoerstellung ist nicht möglich, daher existiert keine Funktionalität zum Löschens eines Kontos.

## Rechte der Nutzer

Die Nutzer haben das Recht auf Auskunft, Berichtigung, Löschung und Einschränkung der Verarbeitung ihrer Daten. Sie haben außerdem das Recht auf Datenübertragbarkeit und das Recht, der Verarbeitung ihrer Daten zu widersprechen.

## Kontakt

Bei Fragen zum Datenschutz können sich die Nutzer an den App-Entwickler wenden.

## Änderungen

Diese Datenschutzhinweise können von Zeit zu Zeit geändert werden. Die aktuelle Version ist immer auf der Website des App-Entwicklers verfügbar.

## Widerspruchsrecht

Nutzer können der Verarbeitung ihrer Daten zu jeder Zeit widersprechen. Der Widerspruch kann an den App-Entwickler gerichtet werden.

## Weitere Informationen

Weitere Informationen zum Datenschutz finden Nutzer auf der Website des App-Entwicklers.

## Links zu anderen Websites

Die App enthält Links zu anderen Websites. Diese Websites unterliegen den Datenschutzbestimmungen der jeweiligen Betreiber.

## Minderjährige

Die App ist nicht für Minderjährige geeignet. Minderjährige sollten die App nur mit Zustimmung ihrer Erziehungsberechtigten nutzen.

## Schlussbestimmungen

Sollten einzelne Bestimmungen dieser Datenschutzhinweise unwirksam sein oder werden, bleiben die übrigen Bestimmungen in ihrer Gültigkeit unberührt.

## Versionshinweis

Diese Datenschutzhinweise wurden zuletzt am 2023-Dez-03 aktualisiert.

## Kontaktinformationen

App-Entwickler: Hassebrauck, Perleberger Str. 13, 10559 Berlin

E-Mail: hassdevelopment [addsign] gmail.com

